# Power Electronics - embedded project

The board files, models, code and schematics contained in the respective directory were used to create a prototype of an embedded power electronics device.
The end product is capable of stepping a DC input down to 3.3V and 5V utillizing the TPS54339 which can provide up to 3A output.
Furthermore, the end product can charge a single-cell Li-Po/Li-Ion battery with a maximum charge current of 2A and use simple LEDs to indicate charge state (charge/fault).
Lastly, the board can be controlled/monitored via an ESP32 functioning as an Access Point.
Fore testing purposes, the DC input for the step-down converters and the charger input lines have been seperated, and so were the input and charger GND planes on the PCB.
If you wish to us a single power-supply for the entire board, just short the "DC Input" and "Charger Input" as well as "GND" and "Earth" planes onto the board.
Alternatively, you can alter the design of the board's DC input stage.
If you wish to skip any redesigning and go directly to reproduction, download the "Gerber" folder which contains all necessary files for PCB manufacturers.
You are free to use the provided schematics, designs and files in general as you wish.
If my work helps with your project(s) in any way, shape or form, I kindly ask you to give deserved credits.